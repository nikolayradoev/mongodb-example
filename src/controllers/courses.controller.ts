import { NextFunction, Request, Response, Router } from "express";
import { StatusCodes } from "http-status-codes";
import { Course } from "../classes/course";
import { CoursesService } from "src/services/courses.service";
import { Service } from "typedi";

@Service()
export class CoursesController {
  router: Router;

  constructor(private coursesService: CoursesService) {
    this.configureRouter();
  }

  private configureRouter(): void {
    this.router = Router();
    /**
    * @swagger
    *
    * definitions:
    *   Course:
    *     type: object
    *     properties:
    *       name:
    *         type: string
    *         description: nom complet du cours
    *       subjectCode:
    *         type: string
    *         description : sigle du cours
    *       credits:
    *         type: number
    *         description: nombre de crédits pour le cours
    *       teacher:
    *         type: string
    *         description: responsable du cours
    */

    /**
     * @swagger
     * tags:
     *   - name: Cours
     *     description: Endpoints pour les cours
     *   - name: Enseignants
     *     description: Endpoints pour les enseignants
     */

    /**
     * @swagger
     *
     * /courses/:
     *   get:
     *     summary: Retourne tous les cours
     *     description: Retourne tous les cours dans la base de données
     *     tags:
     *       - Cours
     *     produces:
     *       - application/json
     *     responses:
     *       200:
     *         description : OK. Si 'null' retourné, aucun cours n'a été trouvé.
     *         content:
     *           application/json:
     *            schema:
     *              type: array
     *              items:
     *                $ref: '#/definitions/Course'
     *              example:
     *                - name: Project I
     *                  subjectCode: INF1900
     *                  credits: 4
     *                  teacher: Jerome Collin
     *                - name: Project II
     *                  subjectCode: LOG2990
     *                  credits: 3
     *                  teacher: Levis Theriault
     *       404:
     *         description : échec de l'opération
     *
     */
    this.router.get(
      "/",
      async (req: Request, res: Response, next: NextFunction) => {
        // this.coursesService
        //   .getAllCourses()
        //   .then((courses: Course[]) => {
        //     res.json(courses);
        //   })
        //   .catch((error: Error) => {
        //     res.status(Httpstatus.NOT_FOUND).send(error.message);
        //   });

        // Can also use the async/await syntax
        try {
          const courses = await this.coursesService.getAllCourses();
          res.json(courses);
        }
        catch (error) {
          res.status(StatusCodes.NOT_FOUND).send(error.message);
        }
      }
    );

    /**
     * @swagger
     *
     * /courses/{subjectCode}:
     *   get:
     *     summary: Retourne un cours spécifique en fonction de son sigle
     *     description: Retourne un cours spécifique en fonction de son sigle (subjectCode)
     *     tags:
     *       - Cours
     *     parameters:
     *       - in: path
     *         name: subjectCode
     *         required: true
     *         type: string
     *         description: sigle du cours à trouver
     *         examples: 
     *          Cours valide:
     *            value: INF1900
     *          Cours invalide:
     *            value: MTH1102
     *     produces:
     *       - application/json
     *     responses:
     *       200:
     *         description : OK. Si 'null' retourné, aucun cours n'a été trouvé.
     *         content:
     *           application/json:
     *            schema:
     *              $ref: '#/definitions/Course'
     *            example:
     *              name: Project I
     *              subjectCode: INF1900
     *              credits: 3
     *              teacher: Jerome Collin
     *       404:
     *         description : échec de l'opération.
     *
     */
    this.router.get(
      "/:subjectCode",
      async (req: Request, res: Response, next: NextFunction) => {
        this.coursesService
          .getCourse(req.params.subjectCode)
          .then((course: Course) => {
            if (!course) {
              res.status(StatusCodes.NOT_FOUND).send();
              return;
            }
            res.json(course);
          })
          .catch((error: Error) => {
            res.status(StatusCodes.NOT_FOUND).send(error.message);
          });
      }
    );

    /**
     * @swagger
     *
     * /courses:
     *   post:
     *     summary: Ajoute un nouveau cours
     *     description: Ajoute un nouveau cours s'il considéré comme valide - moins que 6 crédits et subjectCode débute par LOG ou INF
     *     tags:
     *       - Cours
     *     requestBody:
     *      description: course object
     *      required: true
     *      content:
     *        application/json:
     *          schema:
     *            $ref: '#/definitions/Course'
     *          examples:
     *            Cours valide:
     *              value:       
     *                name: Introduction to programming
     *                subjectCode: INF1007
     *                credits: 4
     *                teacher: Benjamin De Leener
     *            Cours invalide (credits):
     *              value:       
     *                name: Introduction to programming
     *                subjectCode: INF1007
     *                credits: 8
     *                teacher: Benjamin De Leener
     *            Cours invalide (subjectCode):
     *              value:       
     *                name: Calculus II
     *                subjectCode: MTH1102
     *                credits: 2
     *                teacher: Jean Guérin 
     *     responses:
     *       201:
     *         description : Created.
     *       400:
     *         description : échec de l'opération.
     *
     */
    this.router.post(
      "/",
      async (req: Request, res: Response, next: NextFunction) => {
        console.log(req.body);
        this.coursesService
          .addCourse(req.body)
          .then(() => {
            res.status(StatusCodes.CREATED).send();
          })
          .catch((error: Error) => {
            res.status(StatusCodes.BAD_REQUEST).send(error.message);
          });
      }
    );

    /**
     * @swagger
     *
     * /courses:
     *   patch:
     *     summary: Modifie un cours existant
     *     description: Modifie un cours existant. Contrairement à la création d'un cours, il n'y a pas de validation sur les nouvelles valeurs
     *     tags:
     *       - Cours
     *     requestBody:
     *      description: course object
     *      required: true
     *      content:
     *        application/json:
     *          schema:
     *            $ref: '#/definitions/Course'
     *          examples:
     *            Modification des credits et de l'enseignant:
     *              value:       
     *                name: Project II
     *                subjectCode: LOG2990
     *                credits: 4
     *                teacher: Michel Gagnon
     *     responses:
     *       201:
     *         description : Created.
     *       500:
     *         description : échec de l'opération.
     *
     */
    this.router.patch(
      "/",
      async (req: Request, res: Response, next: NextFunction) => {
        this.coursesService
          .modifyCourse(req.body)
          .then(() => {
            res.sendStatus(StatusCodes.OK);
          })
          .catch((error: Error) => {
            res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(error.message);
          });
      }
    );

    /**
     * @swagger
     *
     * /courses/{subjectCode}:
     *   delete:
     *     summary: Supprime un cours spécifique en fonction de son sigle
     *     description: Supprime un cours spécifique en fonction de son sigle (subjectCode)
     *     tags:
     *       - Cours
     *     parameters:
     *       - in: path
     *         name: subjectCode
     *         required: true
     *         type: string
     *         description: sigle du cours à trouver
     *         examples: 
     *          Cours valide:
     *            value: LOG2990
     *          Cours invalide:
     *            value: MTH1102
     *     produces:
     *       - application/json
     *     responses:
     *       204:
     *         description : No Content
     *       404:
     *         description : échec de l'opération.
     *
     */
    this.router.delete(
      "/:subjectCode",
      async (req: Request, res: Response, next: NextFunction) => {
        this.coursesService
          .deleteCourse(req.params.subjectCode)
          .then(() => {
            res.status(StatusCodes.NO_CONTENT).send();
          })
          .catch((error: Error) => {
            console.log(error);
            res.status(StatusCodes.NOT_FOUND).send(error.message);
          });
      }
    );

    /**
     * @swagger
     *
     * /courses/teachers/code/{subjectCode}:
     *   get:
     *     summary: Retourne l'enseignant d'un cours spécifique en fonction de son sigle
     *     description: Retourne l'enseignant d'un cours spécifique en fonction de son sigle (subjectCode)
     *     tags:
     *       - Enseignants
     *     parameters:
     *       - in: path
     *         name: subjectCode
     *         required: true
     *         type: string
     *         description: sigle du cours à trouver
     *         examples: 
     *          Cours valide:
     *            value: LOG2990
     *          Cours invalide:
     *            value: MTH1102
     *     responses:
     *       200:
     *         description : OK
     *         content:
     *           text/plain:
     *            schema:
     *              type: string
     *              example: Levis Theriault 
     *       404:
     *         description : échec de l'opération.
     *
     */
    this.router.get(
      "/teachers/code/:subjectCode",
      async (req: Request, res: Response, next: NextFunction) => {
        this.coursesService
          .getCourseTeacher(req.params.subjectCode)
          .then((teacher: string) => {
            res.send(teacher);
          })
          .catch((error: Error) => {
            res.status(StatusCodes.NOT_FOUND).send(error.message);
          });
      }
    );

    /**
     * @swagger
     *
     * /courses/teachers/name/{name}:
     *   get:
     *     summary: Retourne un cours spécifique en fonction de son enseignant
     *     description: Retourne un cours spécifique en fonction de son enseignant (teacher)
     *     tags:
     *       - Enseignants
     *     parameters:
     *       - in: path
     *         name: name
     *         required: true
     *         type: string
     *         description: sigle du cours à trouver
     *         examples: 
     *          Cours valide:
     *            value: Levis Theriault
     *          Cours invalide:
     *            value: Jean Tremblay
     *     responses:
     *       200:
     *         description : OK
     *         content:
     *           application/json:
     *            schema:
     *              $ref: '#/definitions/Course'
     *            example:
     *              name: Project II
     *              subjectCode: LOG2990
     *              credits: 3
     *              teacher: Levis Theriault
     *       404:
     *         description : échec de l'opération.
     *
     */
    this.router.get(
      "/teachers/name/:name",
      async (req: Request, res: Response, next: NextFunction) => {
        this.coursesService
          .getCoursesByTeacher(req.params.name)
          .then((courses: Course[]) => {
            res.send(courses);
          })
          .catch((error: Error) => {
            res.status(StatusCodes.NOT_FOUND).send(error.message);
          });
      }
    );
  }
}
