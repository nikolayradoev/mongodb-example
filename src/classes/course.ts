export interface Course {
  name: string;
  subjectCode: string;
  credits: number;
  teacher: string;
}
