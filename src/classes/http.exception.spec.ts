import { HttpException } from "./http.exception";
import { StatusCodes } from "http-status-codes";
import { expect } from "chai";
import { describe } from "mocha";

describe("HttpException", () => {
  it("should create a simple HTTPException", () => {
    const createdMessage = "Course created successfuly";
    const httpException: HttpException = new HttpException(StatusCodes.OK, createdMessage);

    expect(httpException.message).to.equals(createdMessage);
  });
});
