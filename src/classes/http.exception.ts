import { StatusCodes } from "http-status-codes";

export class HttpException extends Error {
  public status: number = StatusCodes.INTERNAL_SERVER_ERROR
  constructor(status: number, message: string) {
    super(message);
    this.status = status;
    this.name = "HttpException";
  }
}
