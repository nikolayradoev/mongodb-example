import { Course } from "../classes/course";
import { Collection, Filter, FindOptions, UpdateFilter, WithId } from "mongodb";
import { HttpException } from "src/classes/http.exception";
import { DatabaseService } from "./database.service";
import { Service } from "typedi";

// CHANGE the URL for your database information
const DATABASE_COLLECTION = "courses";

@Service()
export class CoursesService {

  constructor(private databaseService: DatabaseService) {
  }

  get collection(): Collection<Course> {
    return this.databaseService.database.collection(
      DATABASE_COLLECTION
    );
  }

  async getAllCourses(): Promise<Course[]> {
    return this.collection
      .find({})
      .toArray()
      .then((courses: Course[]) => {
        return courses;
      });
  }

  async getCourse(sbjCode: string): Promise<Course> {
    // NB: This can return null if the course does not exist, you need to handle it
    return this.collection
      .findOne({ subjectCode: sbjCode })
      .then((course: WithId<Course>) => {
        return course;
      });
  }

  async addCourse(course: Course): Promise<void> {
    if (this.validateCourse(course)) {
      await this.collection.insertOne(course).catch((error: Error) => {
        throw new HttpException(500, "Failed to insert course");
      });
    } else {
      throw new Error("Invalid course");
    }
  }

  async deleteCourse(sbjCode: string): Promise<void> {
    return this.collection
      .findOneAndDelete({ subjectCode: sbjCode })
      .then((res: WithId<Course>) => {
        if (!res) {
          throw new Error("Could not find course");
        }
      })
      .catch(() => {
        throw new Error("Failed to delete course");
      });
  }

  async modifyCourse(course: Course): Promise<void> {
    let filterQuery: Filter<Course> = { subjectCode: course.subjectCode };
    let updateQuery: UpdateFilter<Course> = {
      $set: {
        subjectCode: course.subjectCode,
        credits: course.credits,
        name: course.name,
        teacher: course.teacher,
      },
    };
    // Can also use replaceOne if we want to replace the entire object
    return this.collection
      .updateOne(filterQuery, updateQuery)
      .then(() => { })
      .catch(() => {
        throw new Error("Failed to update document");
      });
  }

  async getCourseTeacher(sbjCode: string): Promise<string> {
    let filterQuery: Filter<Course> = { subjectCode: sbjCode };
    // Only get the teacher and not any of the other fields
    let projection: FindOptions = { projection: { teacher: 1, _id: 0 } };
    return this.collection
      .findOne(filterQuery, projection)
      .then((course: WithId<Course>) => {
        return course.teacher;
      })
      .catch(() => {
        throw new Error("Failed to get data");
      });
  }
  async getCoursesByTeacher(name: string): Promise<Course[]> {
    let filterQuery: Filter<Course> = { teacher: name };
    return this.collection
      .find(filterQuery)
      .toArray()
      .then((courses: Course[]) => {
        return courses;
      });
  }

  private validateCourse(course: Course): boolean {
    return (
      this.validateCode(course.subjectCode) &&
      this.validateCredits(course.credits)
    );
  }
  private validateCode(subjectCode: string): boolean {
    return subjectCode.startsWith("LOG") || subjectCode.startsWith("INF");
  }
  private validateCredits(credits: number): boolean {
    return credits > 0 && credits <= 6;
  }
}
