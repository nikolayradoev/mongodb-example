import * as chai from "chai";
import { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { describe } from "mocha";
import { MongoClient } from "mongodb";
import { CoursesService } from "./courses.service";
import { Course } from "../classes/course";
import { DatabaseServiceMock } from "./database.service.mock";
chai.use(chaiAsPromised); // this allows us to test for rejection

describe("Courses service", () => {
  let coursesService: CoursesService;
  let databaseService: DatabaseServiceMock;
  let client: MongoClient;
  let testCourse: Course;

  beforeEach(async () => {
    databaseService = new DatabaseServiceMock();
    client = (await databaseService.start()) as MongoClient;
    coursesService = new CoursesService(databaseService as any);
    testCourse = {
      name: "Test Course",
      subjectCode: "LOG1001",
      credits: 1,
      teacher: "John Doe",
    };
    await coursesService.collection.insertOne(testCourse);
  });

  afterEach(async () => {
    await databaseService.closeConnection();
  });

  it("should get all courses from DB", async () => {
    let courses = await coursesService.getAllCourses();
    expect(courses.length).to.equal(1);
    expect(testCourse).to.deep.equals(courses[0]);
  });

  it("should get specific course with valid subjectCode", async () => {
    let course = await coursesService.getCourse("LOG1001");
    expect(course).to.deep.equals(testCourse);
  });

  it("should get null with an invalid subjectCode", async () => {
    let course = await coursesService.getCourse("INF001");
    expect(course).to.deep.equals(null);
  });

  it("should get specific course teacher with valid subjectCode", async () => {
    let teacher = await coursesService.getCourseTeacher("LOG1001");
    expect(teacher).to.equals(testCourse.teacher);
  });

  it("should get specific course based on its teacher", async () => {
    let secondCourse: Course = {
      name: "Test Course 2",
      subjectCode: "LOG1002",
      credits: 1,
      teacher: "Jane Doe",
    };
    coursesService.collection.insertOne(secondCourse);
    const courses = await coursesService.getCoursesByTeacher(
      secondCourse.teacher
    );
    expect(courses.length).to.equals(1);
    expect(courses[0].teacher).to.equals(secondCourse.teacher);
    expect(courses[0].teacher).to.not.equals(testCourse.teacher);
  });

  it("should insert a new course", async () => {
    let secondCourse: Course = {
      name: "Test Course 2",
      subjectCode: "LOG1002",
      credits: 1,
      teacher: "John Doe",
    };

    await coursesService.addCourse(secondCourse);
    let courses = await coursesService.collection.find({}).toArray();
    expect(courses.length).to.equal(2);
    expect(courses.find((x) => x.name === secondCourse.name)).to.deep.equals(
      secondCourse
    );
  });

  it("should not insert a new course if it has an invalid subjectCode and credits", async () => {
    let secondCourse: Course = {
      name: "Test Course 2",
      subjectCode: "CIV1002",
      credits: 9,
      teacher: "John Doe",
    };
    try {
      await coursesService.addCourse(secondCourse);
    } catch {
      let courses = await coursesService.collection.find({}).toArray();
      expect(courses.length).to.equal(1);
    }
  });

  it("should modify an existing course data if a valid subjectCode is sent", async () => {
    let modifiedCourse: Course = {
      name: "Test Course 2",
      subjectCode: "LOG1001",
      credits: 2,
      teacher: "Jane Doe",
    };

    await coursesService.modifyCourse(modifiedCourse);
    let courses = await coursesService.collection.find({}).toArray();
    expect(courses.length).to.equal(1);
    expect(
      courses.find((x) => x.subjectCode === testCourse.subjectCode)?.name
    ).to.deep.equals(modifiedCourse.name);
  });

  it("should not modify an existing course data if no valid subjectCode is passed", async () => {
    let modifiedCourse: Course = {
      name: "Test Course 2",
      subjectCode: "LOG1002",
      credits: 2,
      teacher: "Jane Doe",
    };

    await coursesService.modifyCourse(modifiedCourse);
    let courses = await coursesService.collection.find({}).toArray();
    expect(courses.length).to.equal(1);
    expect(
      courses.find((x) => x.subjectCode === testCourse.subjectCode)?.name
    ).to.not.equals(modifiedCourse.name);
  });

  it("should delete an existing course data if a valid subjectCode is sent", async () => {
    await coursesService.deleteCourse("LOG1001");
    let courses = await coursesService.collection.find({}).toArray();
    expect(courses.length).to.equal(0);
  });

  it("should not delete a course if it has an invalid subjectCode ", async () => {
    try {
      await coursesService.deleteCourse("LOG1002");
    } catch {
      let courses = await coursesService.collection.find({}).toArray();
      expect(courses.length).to.equal(1);
    }
  });

  // Error handling
  describe("Error handling", async () => {
    it("should throw an error if we try to get all courses on a closed connection", async () => {
      await client.close();
      expect(coursesService.getAllCourses()).to.eventually.be.rejectedWith(
        Error
      );
    });

    it("should throw an error if we try to get a specific course on a closed connection", async () => {
      await client.close();
      expect(
        coursesService.getCourse(testCourse.subjectCode)
      ).to.eventually.be.rejectedWith(Error);
    });

    it("should throw an error if we try to delete a specific course on a closed connection", async () => {
      await client.close();
      expect(
        coursesService.deleteCourse(testCourse.subjectCode)
      ).to.eventually.be.rejectedWith(Error);
    });

    it("should throw an error if we try to modify a specific course on a closed connection", async () => {
      await client.close();
      expect(
        coursesService.modifyCourse({} as Course)
      ).to.eventually.be.rejectedWith(Error);
    });

    it("should throw an error if we try to get an invalid courses teacher", async () => {
      expect(
        coursesService.getCourseTeacher("INF0001")
      ).to.eventually.be.rejectedWith(Error, "Failed to get data");
    });

    it("should throw an error if we try to get all courses of a specific teacher on a closed connection", async () => {
      await client.close();
      const newCourse = {
        name: "Test Course #2",
        subjectCode: "INF1001",
        credits: 2,
        teacher: "John Doe",
      };
      expect(coursesService.addCourse(newCourse)).to.eventually.be.rejectedWith(
        Error
      );
    });
  });
});
