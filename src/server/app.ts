import cookieParser from "cookie-parser";
import cors from "cors";
import express from "express";
import swaggerJSDoc from 'swagger-jsdoc';
import * as swaggerUi from 'swagger-ui-express';
import { StatusCodes } from "http-status-codes";
import { CoursesController } from "../controllers/courses.controller";
import { HttpException } from "../classes/http.exception";
import { Service } from "typedi";

@Service()
export class Application {
  app: express.Application;
  private readonly swaggerOptions: swaggerJSDoc.Options;

  constructor(private coursesController: CoursesController) {
    this.app = express();

    this.swaggerOptions = {
      swaggerDefinition: {
        openapi: '3.0.0',
        info: {
          title: 'Exemple MongoDB LOG2990',
          version: '1.0.0',
        },
      },
      apis: ['**/*.ts'],
    };

    this.config();
    this.bindRoutes();
    this.errorHandling();
  }

  private config(): void {
    // Middlewares configuration
    this.app.use(express.json({ limit: "5mb" }));
    this.app.use(express.urlencoded({ limit: "5mb", extended: true }));
    this.app.use(cookieParser());
    this.app.use(cors());
  }

  bindRoutes(): void {
    this.app.use('/api/docs', swaggerUi.serve, swaggerUi.setup(swaggerJSDoc(this.swaggerOptions)));
    this.app.use("/courses", this.coursesController.router);
    this.app.use('/', (req, res) => { res.redirect('/api/docs'); });

  }

  private errorHandling(): void {
    // When previous handlers have not served a request: path wasn't found
    this.app.use(
      (
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
      ) => {
        next(new HttpException(StatusCodes.NOT_FOUND, "Not Found"));
      }
    );

    this.app.use(
      (
        err: HttpException,
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
      ) => {
        // development error handler
        // will print stacktrace
        if (this.app.get("env") === "development") {
          console.error(err.stack);
        }
        res
          .status(err.status || StatusCodes.INTERNAL_SERVER_ERROR)
          .send({ message: err.message });
      }
    );
  }
}
