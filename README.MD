# LOG2990 - Exemple d'utilisation de MongoDB avec NodeJS

## Création d'instance MongoDB avec Cloud Atlas :
 

1. Créez un compte MongoDB : https://www.mongodb.com/cloud/atlas
2. Cliquez sur le bouton **New Project** pour créer un nouveau projet
    1. Donnez un nom à votre projet
    2. Rajoutez vos coéquipiers au projet (ceci est optionnel et peut être fait plus tard)
3. Cliquez sur le bouton **Build a Database** pour créer votre base de données
    1. Choisissez l'option gratuite
    2. Choisissez votre fournisseur et région. L'option AWS en Virginie du Nord par défaut est tout à fait suffisante, mais vous pouvez choisir Google Cloud Platform ou Microsoft Azure
4. Configurez les paramètres de connexion
    1. Ajoutez votre adresse IP avec l'option "Add My Current IP Address" 
    2. Ajoutez l'adresse **0.0.0.0** pour rendre l'instance accessible aux autres machines
    3. Ajoutez un utilisateur pour la base de données. Vous pouvez garder l'utilisateur Admin:admin par défaut
5. Cliquez sur le bouton **Connect** pour obtenir l'URI de votre instance
    1. Choisissez l'option **Connect your application** 
    2. Mettez "Node.js" comme DRIVER et "4.0 or later" comme VERSION si ce n'est pas déjà le cas
    3. Prenez la chaîne de caractères générée pour vous connecter dans la section 2. Vous pouvez y accéder à tout moment en cliquant encore sur **Connect**.
        NB: N'oubliez pas de remplacer \<password\> par le mot de passe de l'utilisateur créé à l'étape 4.3 (les <> doivent être enlevées aussi)


### Création d'une collection dans l'interface Web :

1. Cliquez sur bouton **Browse Collections** du cluster que vous avez créé
2. Choisissez l'option "Add my own data".
3. Entrez le nom de votre "database" et votre "collection" et cliquez sur **Create**



## Description du projet :

Le projet est un serveur HTTP simple avec une connexion vers une base de données MongoDB qui permet d'effectuer plusieurs opérations CRUD (Create Read, Update, Delete). Le projet expose une API accessible à partir de l'URI relatif `/courses/` exposée sur le port `3000`.
    
## Configurations et fichier d'environnement `.env`

Une bonne pratique de programmation est d'éviter d'avoir des variables de configuration directement dans le code source du projet, mais plutôt dans des fichiers de configuration dédiés. Lorsque ces variables contiennent des données sensibles, telles que les informations de connexion à une base de données, les fichiers de configurations sont omis du gestionnaire de version et restent locaux à la machine des développeurs.

Ce projet utilise un fichier nommé `.env` pour sauvegarder les variables de connexion à l'instance MongoDB et le module [dotenv](https://www.npmjs.com/package/dotenv) pour charger le contenu du fichier lors de l'exécution. 


**Attention** : Le fichier `.env` est ignoré par Git à travers la configuration de `.gitignore`.
Vous devez créer le fichier `.env` et y ajouter l'information nécessaire. Le fichier `.env.example` vous est fourni à titre d'exemple du contenu et format nécessaire. 

Avant de pouvoir utiliser le projet, vous devez modifier les variables suivantes:
- Changer la constante DATABASE_URL pour l'URL de votre base de données (voir étape 5.3). S'assurer de mettre le bon nom et mot de passe
- Changer les constantes DATABASE_NAME et DATABASE_COLLECTION pour la database et collection de votre instance MongoDB 

Si vous n'avez pas une collection, suivez les instructions présentes dans ce README.

## Exécution sur Ubuntu 22.04 et libssl.so.1.1

Si vous êtes sur une distribution Linux basée sur Ubuntu 22.04, il se peut que vous n'ayez pas la librairie `libssl1.1` qui est utilisée par MongoDB et MongoMemoryServer. Vous pouvez l'installer manuellement à travers les 2 commandes suivantes :

```bash
wget http://archive.ubuntu.com/ubuntu/pool/main/o/openssl/libssl1.1_1.1.1f-1ubuntu2_amd64.deb
sudo dpkg -i libssl1.1_1.1.1f-1ubuntu2_amd64.deb
```
    
## Génération de données de départ

Vous pouvez populer votre base de données à travers l'interface graphique de MongoDB ou faire une requête GET sur http://localhost:3000/courses/populateDB directement à partir de votre fureteur (voir la fonction `populateDB()` dans `database.service.ts`). Cette fonction génère des documents par défaut qui seront ajoutés à la collection définie dans DATABASE_COLLECTION

Note : localhost designe la machine locale (adresse IP : 127.0.0.1). Si vous essayez de contacter le serveur à partir d'une autre machine, vous devez remplacer "localhost" par l'adresse IP de la machine qui héberge le serveur.

## Description des routes :

Toutes les routes sont disponibles dans `database.controller.ts`. Pour appeler une route, elle doit toujours être précédée par `/courses`

| Route | Description |
| --- | --- |
**GET /** |  Récupère tous les cours sur la BD |
**POST /** | Insère un nouveau cours |
**PATCH /** | Modifie un cours. NB : le cours est modifié en fonction de son subjectCode, mais celui-ci n'est pas passé dans l'URL de la requête. Voir la fonction `modifyCourse` dans `database.service.ts` |
**GET /:subjectCode**   |   Récupère un cours en fonction de son subjectCode (sigle) |
**DELETE /:subjectCode**    |   Supprime un cours en fonction de son subjectCode (sigle) |
**GET /teachers/name/:name**    |   Récupère un cours en fonction du nom de son responsable. NB: utiliser %20 pour remplacer les espaces dans les noms des professeurs. Ex: /courses/teachers/Michel%20Gagnon (%20 remplace l'espace) |
**GET /teachers/code/:subjectCode** |   Récupère le nom du professeur en fonction du subjectCode du cours



## Interaction avec le serveur :

### **Documentation Swagger (méthode recommandée)**

Vous pouvez interagir avec l'ensemble de l'API défini par le serveur à travers une documentation Swagger disponible sur la route [`/api/docs`](http://localhost:300/api/docs).

**Note** : le serveur doit être démarré pour rendre la documentation disponible. N'oubliez pas de le faire à travers la commande `npm start`.

Swagger utilise la spécification [OpenAPI](https://swagger.io/specification/) pour décrire l'ensemble des routes et leur comportement.

Pour chaque route, vous trouverez son nom, les paramètres (si lieu) ainsi que les réponses ainsi que leur contenu (si lieu).

Les paramètres configurables (chemin dynamique, corps de requête) sont accompagnés par des exemples de valeurs valides ou invalides (au besoin).

La capture d'écran suivante présente un exemple de la description de la route `/courses/{subjectCode}`. Un exemple d'une réponse valide présente le format de l'objet retourné.

<img src="https://i.imgur.com/16Qpevm.png">

Vous pouvez essayer les différentes routes directement dans l'interface de Swagger à travers le bouton **Try it out** dans la description de chaque route. Vous pouvez lancer la requête à travers le bouton **Execute** et obtenir la réponse : code HTTP, entêtes et corps (si lieu) par la suite.

Dans le cas de plusieurs routes, telle que `POST /courses`, des exemples prédéfinis sont déjà disponibles pour présenter différents cas. Vous pouvez éditer le contenu des exemples pour modifier les requêtes envoyées :

<img src="https://i.imgur.com/Lox5ahV.png">

<br/>

**Important: Swagger fait de vraies requêtes HTTP vers votre serveur. Vous devez avoir configuré l'accès à votre base de données MongoDB et vous devez garder votre serveur ouvert.**

### Clients HTTP

Vous pouvez interagir avec l'ensemble de l'API défini par le serveur avec un outil comme [Postman](https://www.postman.com/) ou [ThunderClient pour VSCode](https://marketplace.visualstudio.com/items?itemName=rangav.vscode-thunder-client).

[ThunderClient pour VSCode](https://marketplace.visualstudio.com/items?itemName=rangav.vscode-thunder-client) est fortement recommandé puisqu'il est intégré directement dans VSCode et permet de sauvegarder vos requêtes afin de pouvoir les relancer rapidement.

### Directement à travers un navigateur/code JS

Toutes les routes utilisant le verbe GET, sont utilisables en mettant l'adresse directement dans la barre d'URL de votre navigateur.
Exemples : 
    http://localhost:3000/courses/ affichera une page HTML avec tous les cours sur la base de données,
    http://localhost:3000/courses/LOG2990 affichera l'information du cours LOG2990,
    http://localhost:3000/courses/teachers/code/LOG2990 affichera le nom de l'enseignant responsable de LOG2990 (Lévis Thériault)

Pour les routes accessibles par les verbes POST,PATCH et DELETE, l'API Fetch de votre navigateur permet de leur envoyer des requêtes.

Vous n'avez qu'à rentrer le code dans la console du navigateur (accessible avec F12 sur Chrome par exemple)


Exemples :
- Pour POST, le code suivant fera une demande de création d'un nouveau cours INF8460 :
```js
const course = {
    name: "Natural Language Processing",
    credits:3, subjectCode:"INF8460",
    teacher:"Amal Zouaq"
}
fetch("http://localhost:3000/courses/",
    {
        method: 'POST',  
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(course)
    })
```
Si vous faites une demande d'affichage des cours (avec la route `/`), vous verrez le cours rajouté à la liste

Pour DELETE, le code suivant fera une demande de suppression du cours INF8460 (assumant qu'il existe sur la BD) :
```js
fetch("http://localhost:3000/courses/INF8460",
    {
        method: 'DELETE'
    }
) 
```
Si vous faites une demande d'affichage des cours (avec la route `/`), vous verrez le cours n'est plus dans la liste
        
Pour PATCH, le code suivant fera une mise à jour du cours INF8460 (passer de 3 à 4 crédits)
```js
const course = {
    name: "Natural Language Processing",
    credits: 4, 
    subjectCode: "INF8460",
    teacher: "Amal Zouaq"
}
fetch("http://localhost:3000/courses/",
    {
        method: 'PATCH',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(course)
    }
)
```

Si vous faites une demande d'affichage des cours (avec la route `/`), vous verrez le cours INF8460 est désormais à 4 crédits

    
### Ressources supplémentaires

- [Extension ThunderClient](https://marketplace.visualstudio.com/items?itemName=rangav.vscode-thunder-client)
- [Documentation officielle de MongoDB](https://docs.mongodb.com/)